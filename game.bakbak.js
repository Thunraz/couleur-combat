Game = function()
{
    this.config = 'abc';

    /*this.config =
    {
        WALK_VELOCITY: 15,
        MAX_WALK_VELOCITY: 150,
        JUMP_VELOCITY: -250,
        JUMP_INCREMENT: 250,
        ENEMY_SPAWN: 3000,
        SHOOT_INCREMENT: 150,
        BULLET_SPEED: 750
    };*/

    this.player = null;

    this.game = new Phaser.Game(
        800,
        600,
        Phaser.AUTO,
        '',
        {
            preload: this.preload,
            create: this.create,
            update: this.update
        }
    );
}

Game.prototype.preload = function()
{
    this.game.load.tilemap('Level', 'Assets/Level.json', null, Phaser.Tilemap.TILED_JSON);
    this.game.load.image('Tiles', 'Assets/Images/Tiles.png');

    this.game.load.spritesheet('player', 'Assets/Images/player.png', 16, 32, 5);
    this.game.load.image('enemy', 'Assets/Images/Enemy.png');
    this.game.load.image('bullet', 'Assets/Images/Bullet.png');
}

Game.prototype.create = function()
{
    this.game.stage.backgroundColor = '#030';

    this.map = this.game.add.tilemap('Level');
    this.map.addTilesetImage('Tiles');
    this.map.setCollisionByExclusion([ ]);

    this.collisionLayer = this.map.createLayer('CollisionLayer');
    this.collisionLayer.resizeWorld();
    var decorationLayer = this.map.createLayer('DecorationLayer');
    decorationLayer.resizeWorld();

    this.game.physics.gravity.y = 500.0;
    this.game.physics.setBoundsToWorld(true, true, false, true, false);

    console.log(this.config);

    this.player = new Player(12 * 20, 15 * 20, 'player', this.game, this.config);
    //this.game.camera.follow(this.player);
}

Game.prototype.update = function()
{
    this.player.update();

    this.game.physics.collide(this.player.player, [ this.collisionLayer ]);
    //this.game.physics.overlap(bullets, [ collisionLayer, enemies ], bulletOverlapHandler, null, this);
    //this.game.physics.collide(enemies, collisionLayer);
    //this.game.physics.collide(enemies, enemies);
}