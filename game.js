var config =
{
    WALK_VELOCITY: 15,
    MAX_WALK_VELOCITY: 150,
    JUMP_VELOCITY: -250,
    JUMP_INCREMENT: 250,
    ENEMY_SPAWN: 500,
    SHOOT_INCREMENT: 150,
    BULLET_SPEED: 750,
    TILE_SIZE: 20
};

var game;
var map;
var collisionLayer;
var player;
var bullets;
var controls;
var jumpTimer = 0;
var shootTimer = 0;
var enemyTimer = 1000;
var muteTimer = 0;
var enemies;
var currentWeapon = 'R';
var mark;
var gameOver = false;
var gameOverText;
var currentWave = 1;
var playerSpawn;
var enemySpawns;
var mainHud;
var firstRun = true;
var enemiesSpawned = 0;
var newRound = true;

window.onload = function ()
{
    game = new Phaser.Game(800, 600, Phaser.AUTO, 'game', { preload: preload, create: create, update: update });
}

function preload()
{
    game.load.tilemap('Level', 'Assets/Level.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.image('Tiles', 'Assets/Images/Tiles.png');

    game.load.spritesheet('player', 'Assets/Images/player.png', 16, 32, 5);
    game.load.image('enemyR', 'Assets/Images/EnemyR.png');
    game.load.image('enemyG', 'Assets/Images/EnemyG.png');
    game.load.image('enemyB', 'Assets/Images/EnemyB.png');
    game.load.image('bulletR', 'Assets/Images/BulletR.png');
    game.load.image('bulletG', 'Assets/Images/BulletG.png');
    game.load.image('bulletB', 'Assets/Images/BulletB.png');
    
    // HUD components
    game.load.image('mark', 'Assets/Images/Mark.png');
    game.load.image('hud', 'Assets/Images/HUD.png');

    // Audio
    game.load.audio('hitsound', ['Assets/Audio/hit.mp3', 'Assets/Audio/hit.ogg'])
    game.load.audio('jumpsound', ['Assets/Audio/jump.mp3', 'Assets/Audio/jump.ogg'])
    game.load.audio('newroundsound', ['Assets/Audio/nextwave.mp3', 'Assets/Audio/nextwave.ogg'])
    game.load.audio('shootsound', ['Assets/Audio/shoot.mp3', 'Assets/Audio/shoot.ogg'])
    game.load.audio('music', ['Assets/Audio/music.mp3', 'Assets/Audio/music.ogg']);
}

function create()
{
    game.stage.backgroundColor = '#333';

    map = game.add.tilemap('Level');
    map.addTilesetImage('Tiles');
    map.setCollisionByExclusion([ ]);
    
    collisionLayer = map.createLayer('CollisionLayer');
    collisionLayer.resizeWorld();
    var decorationLayer = map.createLayer('DecorationLayer');
    decorationLayer.resizeWorld();

    createSpawns();
    
    game.physics.gravity.y = 500;
    game.physics.setBoundsToWorld();

    player = game.add.sprite(playerSpawn.x * config.TILE_SIZE, playerSpawn.y * config.TILE_SIZE, 'player');
    player.animations.add('left', [0, 1]);
    player.animations.add('right', [3, 4]);
    player.body.bounce.y = 0.1;
    player.body.minVelocity.y = 5;
    player.body.collideWorldBounds = true;
    player.body.mass = 100;

    bullets = game.add.group();
    bullets.setAll('outOfBoundsKill', true);

    game.camera.follow(player);

    // Create input
    controls = game.input.keyboard.createCursorKeys();
    controls.space = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    controls.one = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
    controls.two = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
    controls.three = game.input.keyboard.addKey(Phaser.Keyboard.THREE);
    controls.m = game.input.keyboard.addKey(Phaser.Keyboard.M);

    enemies = game.add.group();
    enemies.setAll('outOfBoundsKill', true);

    var text = 'Kill all enemies!';
    var style =
    {
        font: 'bold 65px Arial',
        fill: '#F00',
        align: 'center',
        stroke: '#FDD',
        strokeThickness: 4
    };
    var t = game.add.text(game.camera.x + game.camera.width / 2, game.camera.y + game.camera.height / 2, text, style);
    t.anchor.setTo(0.5, 0.5);
    game.add.tween(t).to({ alpha: 0}, 2000, Phaser.Easing.Linear.None, true, 1000, false);

    createHUD();

    if(firstRun)
    {
        music = game.add.audio('music');
        music.play('', 0, 1, true);

        firstRun = false;
    }
}

function createHUD()
{
    mainHud = game.add.sprite(0, 0, 'hud');
    mainHud.body.allowGravity = false;
    mainHud.fixedToCamera = true;

    mark = game.add.sprite(2, 2, 'mark');
    mark.body.allowGravity = false;
    mark.fixedToCamera = true;
}

function createSpawns()
{
    playerSpawn = {};
    enemySpawns = [];

    for(var x = 0; x < map.width; x++)
    {
        for(var y = 0; y < map.height; y++)
        {
            var tile = map.getTile(x, y, 'Entities');
            if(tile != null)
            {
                if(tile.index == 1)
                {
                    playerSpawn.x = x;
                    playerSpawn.y = y;
                }
                else if(tile.index == 2)
                {
                    enemySpawns.push({x: x, y: y});
                }
            }
        }
    }
}

function bulletOverlapHandler(bullet, other)
{
    if(other.key && other.key.match('enemy' + bullet.key.match(/^bullet(R|G|B)$/)[1]))
    {
        var hitSound = game.add.audio('hitsound');
        hitSound.play('', 0, 0.4);
        other.destroy();
    }

    bullet.kill();
}

function playerCollisionHandler(player, other)
{
    if(other.key && other.key.match(/^enemy(R|G|B)/))
    {
        player.kill();

        enemies.forEach(function(current)
        {
            current.body.velocity.x = (Math.random() - 0.5) * current.body.maxVelocity.x * 2;
            current.body.velocity.y = (Math.random() - 0.5) * current.body.maxVelocity.y * 2;
        });

        gameOver = true;

        gameOverText = game.add.group();

        var text = 'Game Over';
        var style = { font: 'bold 65px Arial', fill: '#000', align: 'center', stroke: '#666', strokeThickness: 4 };
        var t = game.add.text(
            game.camera.x + game.camera.width / 2,
            game.camera.y + game.camera.height / 2,
            text,
            style
        );
        t.anchor.setTo(0.5, 0.5);
        gameOverText.add(t);

        var text = 'Press Space to restart';
        style.font = 'bold 30px Arial';
        style.strokeThickness = 2;
        t = game.add.text(
            game.camera.x + game.camera.width / 2,
            game.camera.y + game.camera.height / 2 + 50,
            text,
            style
        );
        t.anchor.setTo(0.5, 0.5);
        gameOverText.add(t);
    }
}

function resetGame()
{
    enemies.destroy();
    player.destroy();
    bullets.destroy();
    mainHud.destroy();
    mark.destroy();

    enemiesSpawned = 0;
    currentWave = 1;
    currentWeapon = 'R';
    newRound = true;

    create();
}

function playerInputHandler()
{
    // Left/right walking and idle
    if(controls.left.isDown)
    {
        if(player.body.velocity.x > -config.MAX_WALK_VELOCITY)
            player.body.velocity.x -= config.WALK_VELOCITY;

        player.animations.play('left', 6);
        player.facing = 'left';
    }
    else if(controls.right.isDown)
    {
        if(player.body.velocity.x < config.MAX_WALK_VELOCITY)
            player.body.velocity.x += config.WALK_VELOCITY;

        player.animations.play('right', 6);
        player.facing = 'right';
    }
    else
    {
        player.body.velocity.x *= 0.5;
        player.animations.stop();
        player.frame = 2;
    }

    // Choose weapon
    if(controls.one.isDown)
    {
        currentWeapon = 'R';
        mark.cameraOffset.x = 2;
    }
    else if(controls.two.isDown)
    {
        currentWeapon = 'G';
        mark.cameraOffset.x = 27;
    }
    else if(controls.three.isDown)
    {
        currentWeapon = 'B';
        mark.cameraOffset.x = 52;
    }

    // Jump
    if (controls.up.isDown && player.body.onFloor() && game.time.now > jumpTimer)
    {
        player.body.velocity.y = config.JUMP_VELOCITY;
        jumpTimer = game.time.now + config.JUMP_INCREMENT;

        var jumpSound = game.add.audio('jumpsound');
        jumpSound.play();
    }

    // Shoot
    if(controls.space.isDown && game.time.now > shootTimer)
    {
        var xOffset = (player.facing == 'right' ? player.width : -4);
        var yOffset = player.height - config.TILE_SIZE;

        var bullet = bullets.create(
            player.x + xOffset,
            player.y + yOffset,
            'bullet' + currentWeapon
        );

        var shootSound = game.add.audio('shootsound');
        shootSound.play('', 0, 0.1);

        bullet.body.velocity.y = (Math.random() - 0.5) * 100;
        bullet.body.velocity.x = config.BULLET_SPEED * (player.facing == 'right' ? 1 : -1);
        bullet.body.allowGravity = false;
        
        shootTimer = game.time.now + config.SHOOT_INCREMENT;
    }
}

function spawnEnemies()
{
    var currentWaveEnemies = enemySpawns.length * currentWave;
    if(game.time.now > enemyTimer && newRound)
    {
        enemyTimer = game.time.now + config.ENEMY_SPAWN;

        for(var i = 0; i < enemySpawns.length; i++)
        {
            var enemyColor = ['R', 'G', 'B'][Math.floor(Math.random() * 3)];
            var enemy = enemies.create(
                enemySpawns[i].x * config.TILE_SIZE,
                enemySpawns[i].y * config.TILE_SIZE,
                'enemy' + enemyColor
            );

            enemy.body.bounce.y = 0.1;
            enemy.body.minVelocity.y = 5;
            enemy.body.mass = 100;

            enemiesSpawned++;
        }

        if(enemies.length == currentWaveEnemies)
        {
            newRound = false;
        }
    }

    if(enemies.countLiving() == 0)
    {
        currentWave++;

        var newRoundSound = game.add.audio('newroundsound');
        newRoundSound.play();
        newRound = true;

        var text = 'Stage ' + currentWave;
        var style =
        {
            font: 'bold 65px Arial',
            fill: '#F00',
            align: 'center',
            stroke: '#FDD',
            strokeThickness: 4
        };
        var t = game.add.text(game.camera.x + game.camera.width / 2, game.camera.y + game.camera.height / 2, text, style);
        t.anchor.setTo(0.5, 0.5);
        game.add.tween(t).to({ alpha: 0}, 2000, Phaser.Easing.Linear.None, true, 1000, false);
    }
}

function enemiesFollowPlayer()
{
    enemies.forEachAlive(function(current)
    {
        if(current.x > player.x + config.TILE_SIZE)
        {
            if(current.body.velocity.x > -(config.MAX_WALK_VELOCITY * (0.5 + 0.05 * currentWave)))
                current.body.velocity.x -= config.WALK_VELOCITY;
        }
        else if(current.x < player.x - config.TILE_SIZE)
        {
            if(current.body.velocity.x < config.MAX_WALK_VELOCITY * (0.5 + 0.05 * currentWave))
                current.body.velocity.x += config.WALK_VELOCITY;
        }
        else
        {
            current.body.velocity.x *= 0.8;
        }

        if(current.body.y > player.body.y && current.body.onFloor())
        {
            current.body.velocity.y = config.JUMP_VELOCITY;
        }
    });
}

function update()
{
    if(gameOver)
    {
        if(controls.space.isDown)
        {
            gameOverText.destroy();
            gameOver = false;
            resetGame();
        }
        return;
    }

    if(controls.m.isDown && game.time.now > muteTimer)
    {
        game.sound.mute = !game.sound.mute;
        muteTimer = game.time.now + 100;
    }

    playerInputHandler();

    // Spawn enemies
    spawnEnemies();
    enemiesFollowPlayer();

    game.physics.collide(player, [ collisionLayer, enemies ], playerCollisionHandler);
    game.physics.overlap(bullets, [ collisionLayer, enemies ], bulletOverlapHandler, null, this);
    game.physics.collide(enemies, collisionLayer);
    game.physics.collide(enemies, enemies);
}